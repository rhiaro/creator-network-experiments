import json
import requests
from bs4 import BeautifulSoup
import uuid
from urlparse_plus import get_tlds, DomainParts, get_domain_parts
from rdflib import Graph, Literal, BNode, Namespace, RDF, URIRef
from rdflib.namespace import FOAF

def choose_feed(format="json"):
	# At some point have params for more options.
	# For now return top favourites from this week.
	return "http://gdata.youtube.com/feeds/api/standardfeeds/top_favorites?alt=" + format + "&orderby=published&time=this_week"

def get_feed(url):
	r = requests.get(choose_feed())
	data = json.loads(r.text)
	return data

def get_user_ids_from_feed(feed):
	ids = []
	entries = feed['feed']['entry']
	for e in entries:
		ids.append(e['media$group']['yt$uploaderId']['$t'])

	return ids

class YoutubeUser(object):

	def __init__(self, userid):
		self.userid = userid
		self.profile = "http://youtube.com/channel/"+self.userid+"/about?alt=json"
		#self.profile = "http://youtube/user/06085668/about?alt=json"
		#eg.id: UCDTGnb10B_A0rnnJMLW9Bmw

		self.links = self.set_links()
		self.uuid = uuid.uuid4()
		self.uri = URIRef("http://rhiaro.co.uk/cc/onlinepersona/"+str(self.uuid))
		self.graph = self.make_graph()

	def set_links(self):
		return self.find_links(self.parse_profile())

	def get_profile(self):
		url = self.profile
		profile = requests.get(url)
		profile_html = profile.text
		return profile_html

	def parse_profile(self):
		html = self.get_profile()
		parsed = BeautifulSoup(html)
		return parsed

	def find_links(self, parsed_profile):
		links = []
		try:
			l = parsed_profile.find_all('li', class_="custom-links-item")
			for li in l:
				a = li.find('a')
				url = a.get('href')
				links.append(url)
		except:
			pass # No links, who cares, move on
		# Links might appear in more than one place on the profile
		unique = list(set(links))
		return unique

	def make_graph(self):
		g = Graph()
		bind_namespaces(g)
		# Add OnlinePersona and this YT account to the graph
		yt_account_uri = URIRef("http://rhiaro.co.uk/cc/onlineaccount/youtube-"+str(self.userid))
		g.add( (self.uri, RDF.type, FOAF.Agent) )
		g.add( (self.uri, FOAF.account, yt_account_uri) )
		g.add( (yt_account_uri, FOAF.accountServiceHomepage, URIRef("http://youtube.com")) )
		g.add( (yt_account_uri, FOAF.accountName, Literal(self.userid)) )

		# Add linked accounts to graph
		# TODO: Get better at working out which bit of the URL is the account name
		for link in self.links:
			parsed_link = get_domain_parts(link)
			if is_known_account_service(link):
				if service_uses_subdomains_for_users(link):
					# Just assume the first one is the account name for now..
					account_name = parsed_link.subdomains[0]
				else:
					# Assume the path is the account name
					account_name = parsed_link.urlparsed.path[1:] # Remove / at start
				
				homepage = "http://" + parsed_link.domain + "." + parsed_link.tld

				link_account_uri = URIRef("http://rhiaro.co.uk/cc/onlineaccount/"+parsed_link.domain+"-"+account_name)

				g.add( (self.uri, FOAF.account, link_account_uri) )
				g.add( (link_account_uri, FOAF.accountServiceHomepage, URIRef(homepage)) )
				g.add( (link_account_uri, FOAF.accoutName, Literal(account_name)) )
			else: # For now...
				g.add( (self.uri, META.has_unknown_link, URIRef(link)))

		return g

def is_known_account_service(url):
	# Start with YouTube's list of services
	known_services = ["twitter", "facebook", "fb", "play.google", "plus.google", "google", "myspace", "tumblr", "blogger", "blogspot", "deviantart", "wordpress", "soundcloud", "orkut", "flickr", "itunes", "pinterest", "instagram", "zazzle", "spreadshirt", "cafepress", "linkedin", "newgrounds", "bandcamp", "youtube"]
	urlparts = get_domain_parts(url)
	urlparts = get_google_account_base(urlparts)
	service = urlparts.domain
	return service in known_services

def service_uses_subdomains_for_users(url):
	# To be expanded...
	services_that_use_subdomains = ["bandcamp", "wordpress", "blogger", "blogspot", "tumblr"]
	urlparts = get_domain_parts(url)
	service = urlparts.domain
	return url in services_that_use_subdomains

def get_google_account_base(url):
	# Cos of all it's annoying subdomains for stuff
	try: 
		d = url.domain
		urlparts = url
	except(AttributeError):
		urlparts = get_domain_parts(url)

	if urlparts.domain == "google" and urlparts.subdomains:
		urlparts.domain = urlparts.subdomains[0] + ".google"
	
	return urlparts

def bind_namespaces(graph):
	graph.bind("foaf", FOAF)
	graph.bind("rdf", RDF)
	global OOCC
	OOCC = Namespace("http://rhiaro.co.uk/vocab/oocc#")
	graph.bind("oocc", OOCC)
	global META 
	META = Namespace("http://rhiaro.co.uk/vocab/meta#")
	graph.bind("meta", META)


if __name__ == '__main__':
	response = get_feed(choose_feed())
	ids = get_user_ids_from_feed(response)
	users = []

	# Make YoutubeUser objects
	for u in ids:
		# Keep a list of all the users we find, regardless of whether they have links
		users.append(YoutubeUser(u))

	for u in users:
		print u.graph.serialize(format="turtle")
	
