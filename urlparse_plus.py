from __future__ import with_statement
from urlparse import urlparse

def get_tlds(path="", filename="effective_tld_names.dat.txt"):
    # load tlds, ignore comments and empty lines:
    tld_file = path + filename
    with open(tld_file) as tldFile:
        tlds = [line.strip() for line in tldFile if line[0] not in "/\n"]
    return tlds

class DomainParts(object):
    def __init__(self, domain_parts, tld, urlparsed):
        self.domain = None
        self.subdomains = None
        self.tld = tld
        self.urlparsed = urlparsed
        if domain_parts:
            self.domain = domain_parts[-1]
            if len(domain_parts) > 1:
                self.subdomains = domain_parts[:-1]

def get_domain_parts(url, tlds=get_tlds()):
    urlparsed = urlparse(url)
    url_elements = urlparsed.hostname.split('.')
    # url_elements = ["abcde","co","uk"]
    for i in range(-len(url_elements),0):
        last_i_elements = url_elements[i:]
        #    i=-3: ["abcde","co","uk"]
        #    i=-2: ["co","uk"]
        #    i=-1: ["uk"] etc

        candidate = ".".join(last_i_elements) # abcde.co.uk, co.uk, uk
        wildcard_candidate = ".".join(["*"]+last_i_elements[1:]) # *.co.uk, *.uk, *
        exception_candidate = "!"+candidate

        # match tlds: 
        if (exception_candidate in tlds):
            return ".".join(url_elements[i:]) 
        if (candidate in tlds or wildcard_candidate in tlds):
            return DomainParts(url_elements[:i], '.'.join(url_elements[i:]), urlparsed)
            # returns ["abcde"]

    raise ValueError("Domain not in global list of TLDs")

if __name__ == '__main__':
    print "Example..."
    url = "http://sub2.sub1.example.co.uk:80"
    domain_parts = get_domain_parts(url,get_tlds())
    print "Full:", url
    print "Domain:", domain_parts.domain
    print "Subdomains:", domain_parts.subdomains or "None"
    print "TLD:", domain_parts.tld
    print "Rest:", domain_parts.urlparsed